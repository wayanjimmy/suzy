export function initSubject() {
  const subject = {
    id: '',
    name: '',
    imageFile: null,
    image: '',
    type: '',
    group: '',
    gender: '',
    chipNumber: '',
    tatouNumber: '',
    specialID: '',
    rescuedAt: '',
    rescuePlace: '',
    rehabLocation: '',
    diet: '',
    supplement: '',
    exterior: '',
    behaviour: '',
  }
  return subject
}
