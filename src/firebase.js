import firebase from 'firebase'

const config = {
  apiKey: 'AIzaSyC0yc66BLGX4ETxK90FOBd1T0_OXiK9Ab4',
  authDomain: 'suzy-d2641.firebaseapp.com',
  databaseURL: 'https://suzy-d2641.firebaseio.com',
  projectId: 'suzy-d2641',
  storageBucket: 'suzy-d2641.appspot.com',
  messagingSenderId: '516112208091',
}

firebase.initializeApp(config)

export default firebase

export const database = firebase.database()
export const auth = firebase.auth()
export const storage = firebase.storage()
export const googleAuthProvider = new firebase.auth.GoogleAuthProvider()
