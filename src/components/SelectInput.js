import { h } from 'preact'

const SelectInput = ({
  name,
  label,
  onChange,
  defaultOption,
  placeholder,
  value,
  options,
}) => (
  <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <select
      name={name}
      value={value}
      onChange={onChange}
      className="form-control"
    >
      <option value="">{defaultOption}</option>
      {options.map(option => (
        <option key={option.value} value={option.value}>
          {option.text}
        </option>
      ))}
    </select>
  </div>
)

export default SelectInput
