import { h } from 'preact'

const TextInput = ({ name, label, onChange, placeholder, value }) => (
  <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <input
      type="text"
      name={name}
      className="form-control"
      id={name}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
    />
  </div>
)

export default TextInput
