import { h } from 'preact'

const FileInput = ({ name, label, onChange, value }) => (
  <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <input
      type="file"
      name={name}
      className="form-control"
      id={name}
      value={value}
      onChange={onChange}
      accept="image/*"
    />
  </div>
)

export default FileInput
