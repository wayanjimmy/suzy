import { h } from 'preact'
import { Router } from 'preact-router'
import Home from './Home'

const Main = () => (
  <main className="col-sm-9 ml-sm-auto col-md-10 pt-3" role="main">
    <Router onChange={this.handleRoute}>
      <Home path="/" />
    </Router>
  </main>
)

export default Main
