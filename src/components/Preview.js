import { h } from 'preact'

const Preview = ({ subject }) => (
  <dl class="row">
    <dt class="col-sm-3">Nama</dt>
    <dt class="col-sm-9">{subject.name}</dt>
    <dt class="col-sm-3">Jenis</dt>
    <dt class="col-sm-9">{subject.type}</dt>
    <dt class="col-sm-3">Group</dt>
    <dt class="col-sm-9">{subject.group}</dt>
    <dt class="col-sm-3">Gender</dt>
    <dt class="col-sm-9">{subject.gender}</dt>
    <dt class="col-sm-3">Chip No.</dt>
    <dt class="col-sm-9">{subject.chipNumber}</dt>
    <dt class="col-sm-3">Tatou Number</dt>
    <dt class="col-sm-9">{subject.tatouNumber}</dt>
    <dt class="col-sm-3">Special ID.</dt>
    <dt class="col-sm-9">{subject.specialID}</dt>
    <dt class="col-sm-3">Rescued At</dt>
    <dt class="col-sm-9">{subject.rescuedAt}</dt>
    <dt class="col-sm-3">Rescue Place</dt>
    <dt class="col-sm-9">{subject.rescuePlace}</dt>
    <dt class="col-sm-3">Rehab Location</dt>
    <dt class="col-sm-9">{subject.rehabLocation}</dt>
    <dt class="col-sm-3">Diet</dt>
    <dt class="col-sm-9">{subject.diet}</dt>
    <dt class="col-sm-3">Supplement</dt>
    <dt class="col-sm-9">{subject.supplement}</dt>
    <dt class="col-sm-3">Exterior</dt>
    <dt class="col-sm-9">{subject.exterior}</dt>
    <dt class="col-sm-3">Behaviour</dt>
    <dt class="col-sm-9">{subject.behaviour}</dt>
  </dl>
)

export default Preview
