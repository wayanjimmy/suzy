import { h, Component } from 'preact'
import Header from './Header'
import Main from './Main'

class App extends Component {
  handleRoute = e => (this.currentUrl = e.url)

  render() {
    return (
      <div>
        <Header title="Suzy" />
        <div className="container-fluid">
          <div className="row">
            <Main />
          </div>
        </div>
      </div>
    )
  }
}

export default App
