import { h } from 'preact'
import TextInput from './TextInput'
import TextAreaInput from './TextAreaInput'
import SelectInput from './SelectInput'
import DateInput from './DateInput'
import FileInput from './FileInput'

const genderOptions = [
  {
    value: 'male',
    text: 'Male',
  },
  {
    value: 'female',
    text: 'Female',
  },
]

const SubjectForm = ({ subject, loading, onChange, onImageChange, onSave }) => (
  <form>
    <TextInput
      name="name"
      label="Name"
      placeholder="Name"
      onChange={onChange}
      value={subject.name}
    />
    {subject.id !== '' && (
      <FileInput name="image" label="Image" onChange={onImageChange} />
    )}
    <TextInput
      name="type"
      label="Type"
      placeholder="Type"
      onChange={onChange}
      value={subject.type}
    />
    <TextInput
      name="group"
      label="Group"
      placeholder="Group"
      onChange={onChange}
      value={subject.group}
    />
    <SelectInput
      name="gender"
      label="Gender"
      placeholder="Gender"
      defaultOption="- Select Gender -"
      options={genderOptions}
      value={subject.gender}
      onChange={onChange}
    />
    <TextInput
      name="chipNumber"
      label="Chip No."
      placeholder="Chip No."
      onChange={onChange}
      value={subject.chipNumber}
    />
    <TextInput
      name="tatouNumber"
      label="Tatou No."
      placeholder="Tatou No."
      onChange={onChange}
      value={subject.tatouNumber}
    />
    <TextInput
      name="specialID"
      label="Special ID"
      placeholder="Special ID"
      onChange={onChange}
      value={subject.specialID}
    />
    <DateInput
      name="rescuedAt"
      label="Rescued At"
      placeholder="Rescued At"
      onChange={onChange}
      value={subject.rescuedAt}
    />
    <TextInput
      name="rescuePlace"
      label="Rescue Place"
      placeholder="Rescue Place"
      onChange={onChange}
      value={subject.rescuePlace}
    />
    <TextInput
      name="diet"
      label="Diet"
      placeholder="Diet"
      onChange={onChange}
      value={subject.diet}
    />
    <TextInput
      name="supplement"
      label="Supplement"
      placeholder="Supplement"
      onChange={onChange}
      value={subject.supplement}
    />
    <TextInput
      name="exterior"
      label="Exterior"
      placeholder="Exterior"
      onChange={onChange}
      value={subject.exterior}
    />
    <TextAreaInput
      name="behaviour"
      label="Behaviour"
      placeholder="Behaviour"
      onChange={onChange}
      value={subject.behaviour}
    />
    <button
      type="submit"
      className="btn btn-primary"
      onClick={onSave}
      disabled={loading}
    >
      {loading ? 'Saving...' : 'Save'}
    </button>
  </form>
)

export default SubjectForm
