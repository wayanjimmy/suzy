import { h } from 'preact'

const TextAreaInput = ({
  name,
  label,
  onChange,
  placeholder,
  value,
  rows,
  cols,
}) => (
  <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <textarea
      type="text"
      name={name}
      className="form-control"
      id={name}
      placeholder={placeholder}
      value={value}
      onChange={onChange}
      rows={rows}
      cols={cols}
    />
  </div>
)

export default TextAreaInput
