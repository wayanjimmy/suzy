import { h, Component } from 'preact'

const Header = ({ title }) => (
  <nav className="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
    <a className="navbar-brand" style={{ color: 'white' }}>
      {title}
    </a>
    <button className="navbar-toggler d-lg-none" type="button">
      <span className="navbar-toggler-icon" />
    </button>
  </nav>
)

export default Header
