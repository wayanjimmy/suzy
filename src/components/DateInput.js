import { h } from 'preact'

const DateInput = ({ name, label, onChange, placeholder, value }) => (
  <div className="form-group">
    <label htmlFor={name}>{label}</label>
    <input
      type="date"
      name={name}
      className="form-control"
      id={name}
      value={value}
      placeholder={placeholder}
      onChange={onChange}
    />
  </div>
)

export default DateInput
