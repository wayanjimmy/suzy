import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import { Edit, Eye } from 'react-feather'
import { auth, database, storage } from '../firebase'
import Login from './Login'
import SubjectForm from './SubjectForm'
import Preview from './Preview'
import { Row, H1, MarginBottom } from '../style'
import * as utils from '../utils'
import uuid from 'uuid/v4'

class Home extends Component {
  state = {
    preview: false,
    imageUrl: '',
    subject: utils.initSubject(),
    subjects: {},
    currentUser: null,
  }

  onPreviewLeave = e => {
    e.preventDefault()

    this.setState({ preview: false })
  }

  onPreview = id => e => {
    e.preventDefault()
    const subject = this.state.subjects[id]
    subject.id = id
    this.setState({
      preview: true,
      subject,
    })
  }

  onEditSubject = id => e => {
    e.preventDefault()

    const subject = this.state.subjects[id]
    subject.id = id
    if (subject.image !== '') {
      storage
        .ref()
        .child(subject.image)
        .getDownloadURL()
        .then(url => this.setState({ imageUrl: url }))
    } else {
      this.setState({ imageUrl: '' })
    }
    this.setState({ subject })
  }

  onUpdateSubjectState = e => {
    const field = e.target.name
    const { subject } = this.state
    subject[field] = e.target.value
    this.setState({ subject })
  }

  onImageChange = e => {
    const files = e.target.files
    if (files) {
      const file = files[0]
      const fileName = e.target.value
      const ext = fileName.substr(fileName.lastIndexOf('.') + 1)
      const { subject } = this.state
      storage
        .ref()
        .child(`images/${subject.id}.${ext}`)
        .put(file)
        .then(snapshot => {
          const { currentUser } = this.state
          database.ref(currentUser.uid + '/subjects/' + subject.id).set({
            ...subject,
            image: snapshot.metadata.fullPath,
          })
        })
    }
  }

  onSaveSubject = e => {
    e.preventDefault()

    const { currentUser } = this.state

    if (this.state.subject.id === '') {
      const subject = {
        ...this.state.subject,
        id: uuid(),
      }
      database.ref(currentUser.uid + '/subjects/' + subject.id).set(subject)
    } else {
      const { subject } = this.state
      database.ref(currentUser.uid + '/subjects/' + subject.id).set(subject)
    }
  }

  componentDidMount() {
    auth.onAuthStateChanged(currentUser => {
      if (currentUser) {
        const subjectRef = database.ref(currentUser.uid + '/subjects')
        subjectRef.on('value', snapshot => {
          if (snapshot.val()) {
            this.setState({ subjects: snapshot.val() })
          }
        })
        this.setState({ currentUser })
      }
    })
  }

  render() {
    const { preview, imageUrl, currentUser, subjects, subject } = this.state
    const subjectKeys = Object.keys(subjects)

    return (
      <div>
        {!currentUser ? (
          <Login />
        ) : (
          <div>
            {!preview ? (
              <div>
                <Row>
                  <div className="col-12">
                    <table className="table">
                      <thead>
                        <tr>
                          <th scope="col">#</th>
                          <th scope="col">Name</th>
                          <th scope="col">Gender</th>
                          <th scope="col">Type</th>
                          <th scope="col">Action</th>
                        </tr>
                      </thead>
                      <tbody>
                        {subjectKeys.map((key, index) => {
                          const subject = subjects[key]
                          return (
                            <tr key={key}>
                              <th scope="row">{index + 1}</th>
                              <td>{subject.name}</td>
                              <td>{subject.gender}</td>
                              <td>{subject.type}</td>
                              <td>
                                <button
                                  className="btn btn-link"
                                  type="button"
                                  style={{ cursor: 'pointer' }}
                                  onClick={this.onEditSubject(key)}
                                >
                                  <Edit />
                                </button>
                                <button
                                  className="btn btn-link"
                                  type="button"
                                  style={{ cursor: 'pointer' }}
                                  onClick={this.onPreview(key)}
                                >
                                  <Eye />
                                </button>
                              </td>
                            </tr>
                          )
                        })}
                      </tbody>
                    </table>
                  </div>
                </Row>
                <Row>
                  <MarginBottom className="col-6">
                    {imageUrl !== '' && <img src={imageUrl} />}
                    <SubjectForm
                      loading={false}
                      subject={subject}
                      onChange={this.onUpdateSubjectState}
                      onImageChange={this.onImageChange}
                      onSave={this.onSaveSubject}
                    />
                  </MarginBottom>
                </Row>
              </div>
            ) : (
              <Row>
                <Preview subject={subject} />
                <a onClick={this.onPreviewLeave}>{'<'}</a>
              </Row>
            )}
          </div>
        )}
      </div>
    )
  }
}

export default Home
