import { h, Component } from 'preact'
import { auth, googleAuthProvider } from '../firebase'
import { FormLogin } from '../style'

class Login extends Component {
  onLogin = e => {
    e.preventDefault()
    auth.signInWithPopup(googleAuthProvider)
  }

  render() {
    return (
      <div>
        <FormLogin>
          <form>
            <button
              onClick={this.onLogin}
              className="btn btn-lg btn-primary btn-block"
            >
              Login
            </button>
          </form>
        </FormLogin>
      </div>
    )
  }
}

export default Login
