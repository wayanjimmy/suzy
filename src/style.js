import glamorous from 'glamorous/preact'

export const FormLogin = glamorous.div({
  maxWidth: '330px',
  padding: '15px',
  margin: '0 auto',
})

export const H1 = glamorous.h1({
  marginBottom: '20px',
  paddingBottom: '9px',
  borderBottom: '1px solid #eee',
})

export const Row = glamorous.div('row')

export const MarginBottom = glamorous.span({
  marginBottom: '30px',
})
